import socket
import struct
import crc
import binascii


# Frame format
#             | Start | Frame Code | Angle0 | Angle1 | ... | Angle11 | CRC |
# size(bytes) |     1 |          1 |      4 |      4 | ... |       4 |  2  |
# value       |  0xff |            |        |        | ... |         |     |

BUFFER_SIZE = 16


class TCP_Client:
    def __init__(self, target_ip, target_port):
        self.target_ip = target_ip
        self.target_port = target_port

        self.crc_config = crc.Configuration(
            width=16,
            polynomial=0x1021,
            init_value=0x00,
            final_xor_value=0x00,
            reverse_input=False,
            reverse_output=False,
        )
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_socket.connect((target_ip, target_port))

    def send_angles(self, angle_array):
        data_bytes = struct.pack(
            ">BBffffffffffff",
            0xFF,
            0x64,
            angle_array[0],
            angle_array[1],
            angle_array[2],
            angle_array[3],
            angle_array[4],
            angle_array[5],
            angle_array[6],
            angle_array[7],
            angle_array[8],
            angle_array[9],
            angle_array[10],
            angle_array[11],
        )
        calculator = crc.Calculator(self.crc_config)
        crc_bytes = struct.pack(">H", calculator.checksum(data_bytes))
        data_frame = data_bytes + crc_bytes
        self.send_bytes(data_frame)

    def send_bytes(self, byte_array):
        self.tcp_socket.send(byte_array)

    def receive_bytes(self):
        frame = self.tcp_socket.recv(BUFFER_SIZE)
        return frame

    def receive_orientation_angles(self):
        data_bytes = []
        angles = []
        frame = self.receive_bytes()
        calculator = crc.Calculator(self.crc_config)
        if len(frame) == BUFFER_SIZE and frame[0] == 0xFF and frame[1] == 0x65:
            data_bytes = struct.pack(
                ">BBBBBBBBBBBBBB",
                frame[0],
                frame[1],
                frame[2],
                frame[3],
                frame[4],
                frame[5],
                frame[6],
                frame[7],
                frame[8],
                frame[9],
                frame[10],
                frame[11],
                frame[12],
                frame[13],
            )
            crc_c = calculator.checksum(data_bytes)
            crc_proper = (frame[14] << 8) | frame[15]
            if crc_c == crc_proper:
                angles = self.calculate_angles(frame, 2)
                return angles

    def calculate_angles(self, frame, start_index):
        angles = []
        for i in range(start_index, start_index + 12, 4):
            float_bytes = bytes(frame[i : i + 4])
            float_value = struct.unpack(">f", float_bytes)[0]
            angles.append(float_value)
        return angles
